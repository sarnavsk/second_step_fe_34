const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const newer = require('gulp-newer');

function distImg() {
    return gulp.src('src/img/**')
        .pipe(newer('dist/img'))
        .pipe(imagemin({
            verbose: true
        }))
        .pipe(gulp.dest('dist/img'))
}

exports.imgReady = distImg;